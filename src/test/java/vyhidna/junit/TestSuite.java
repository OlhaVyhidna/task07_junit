package vyhidna.junit;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import vyhidna.junit.minesweeper.MainTest;
import vyhidna.junit.minesweeper.controller.MinesweeperControllerTest;
import vyhidna.junit.minesweeper.model.MinesweeperTest;
import vyhidna.junit.minesweeper.view.MinesweeperViewTest;
import vyhidna.junit.plateau.PlateauTest;

@RunWith(JUnitPlatform.class)
@SelectClasses({
    MinesweeperControllerTest.class,
    MinesweeperTest.class,
    MinesweeperViewTest.class,
    MainTest.class,
    PlateauTest.class})
public class TestSuite {


}
