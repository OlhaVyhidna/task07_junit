package vyhidna.junit.minesweeper.model;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

public class MinesweeperTest {

  private  Minesweeper minesweeper;
  private  final int fieldLength = 4;
  private  final int fieldHeight = 4;

  @BeforeEach
  public  void initMinesweeperTest() {
    minesweeper = new Minesweeper(fieldHeight, fieldLength, 2);
  }

  @Test
  public void doesGetMinesweeperFieldMethodDoesReturnNull() {
    assertFalse(minesweeper.getMinesweeperField() == null);
  }

  @RepeatedTest(2)
  public void doesGetMinesweeperFieldMethodReturnFieldOfCorrectHeight() {
    System.out.println(minesweeper.getMinesweeperField().length);
    assertTrue(minesweeper.getMinesweeperField().length == fieldHeight);
  }

  @Test
  public void doesSetMinesweeperFieldMethodSetNewValue(){
    char[][] newArray = new char[6][6];
    minesweeper.setMinesweeperField(newArray);
    assertEquals(minesweeper.getMinesweeperField(), newArray);
  }



}
