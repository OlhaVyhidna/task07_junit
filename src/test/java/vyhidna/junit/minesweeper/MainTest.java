package vyhidna.junit.minesweeper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import vyhidna.junit.minesweeper.model.Minesweeper;

public class MainTest {

  private static Main main;
  private static final int fieldSize = 3;

  @Mock
  private static Scanner scanner;

  @BeforeAll
  public static void initMain(){
    main = new Main();
    Scanner scanner = mock(Scanner.class);
    when(scanner.nextInt()).thenReturn(fieldSize);
    main.setScanner(scanner);
  }

  @Test
  public void  doesInitMinesweeperReturnObject()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Class<? extends Main> aClass = main.getClass();
    Method initMinesweeper = aClass.getDeclaredMethod("initMinesweeper");
    initMinesweeper.setAccessible(true);
    Minesweeper invoke = (Minesweeper) initMinesweeper.invoke(main);
    assertNotNull(invoke);
  }

  @Test
  public void  doesInitMinesweeperReturnCorrectNewMinesweeperObject()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Class<? extends Main> aClass = main.getClass();
    Method initMinesweeper = aClass.getDeclaredMethod("initMinesweeper");
    initMinesweeper.setAccessible(true);
    Minesweeper invoke = (Minesweeper) initMinesweeper.invoke(main);
    assertEquals(3, invoke.getMinesweeperField().length);
  }

  @AfterAll
  public static void testPrivateStaticFinalConstant()
      throws NoSuchFieldException, IllegalAccessException {
    Class<? extends Main> aClass = main.getClass();
    Field string_for_test = aClass.getDeclaredField("STRING_FOR_TEST");
    string_for_test.setAccessible(true);
    assertSame("String for test", string_for_test.get(Main.class));
  }

}
