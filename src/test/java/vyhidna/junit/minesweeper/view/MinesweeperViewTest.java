package vyhidna.junit.minesweeper.view;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MinesweeperViewTest {

  private static MinesweeperView minesweeperView;

  @BeforeAll
  public static void initFields() {
    minesweeperView = new MinesweeperView();
  }

  @Test
  public void checkPrintMinesweeperField() {
    char[][] arrayForChecking = new char[][]{{'q', 'w', 'e'}, {'r', 't', 'y'}, {'u', 'i', 'o'}};
    minesweeperView.printMinesweeperField(arrayForChecking);
    System.out.println("printMinesweeperField method works properly");

  }
}
