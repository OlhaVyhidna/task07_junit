package vyhidna.junit.minesweeper.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import vyhidna.junit.minesweeper.model.Minesweeper;
import vyhidna.junit.minesweeper.view.MinesweeperView;

public class MinesweeperControllerTest {

  private static Minesweeper mockMinesweeper;
  private static MinesweeperView minesweeperView;
  private static MinesweeperController minesweeperController;

  @BeforeEach
  public  void initFields() {
    char[][] minesweeperField = new char[][]{{'-', '-', '*', '-'}, {'*', '-', '*', '-'},
        {'-', '-', '-', '-'}
        , {'-', '*', '-', '-'}};
    mockMinesweeper = mock(Minesweeper.class);
    when(mockMinesweeper.getMinesweeperField()).thenReturn(minesweeperField);
    minesweeperView = new MinesweeperView();
    minesweeperController = new MinesweeperController(mockMinesweeper, minesweeperView);
  }

  @Test
  public void checkCreateIntermediatePlayingField()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    minesweeperController.setIntermediatePlayingField(null);
    assertNull(minesweeperController.getIntermediatePlayingField());
    System.out.println("Field before initialisation equals null");
    Class<? extends MinesweeperController> aClass = minesweeperController.getClass();
    Method createIntermediatePlayingField = aClass
        .getDeclaredMethod("createIntermediatePlayingField");
    createIntermediatePlayingField.setAccessible(true);
    createIntermediatePlayingField.invoke(minesweeperController);
    System.out.println("Field after initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out.println("createIntermediatePlayingField method works properly");
  }

  @Test
  public void verifyMinesweeperMethod(){
    verify(mockMinesweeper).getMinesweeperField();
    System.out.println("getMinesweeperField method was called");
  }

  @Test
  public void checkDefaultInitIntermediatePlayingField()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    minesweeperController.setIntermediatePlayingField(new char[6][6]);
    System.out.println("Field before initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    Class<? extends MinesweeperController> aClass = minesweeperController.getClass();
    Method defaultInitIntermediatePlayingField = aClass
        .getDeclaredMethod("defaultInitIntermediatePlayingField");
    defaultInitIntermediatePlayingField.setAccessible(true);
    defaultInitIntermediatePlayingField.invoke(minesweeperController);
    System.out.println("Field after initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out.println("defaultInitIntermediatePlayingField method works properly");
  }

  @Test
  public void checkInitIntermediatePlayingFieldWithMinesweeperFieldValuesMethod() {
    System.out.println("Field before initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out.println("Print in method");
    minesweeperController.initIntermediatePlayingFieldWithMinesweeperFieldValues();
    assertNotNull(minesweeperController.getIntermediatePlayingField());
    System.out.println("Field after initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out
        .println("initIntermediatePlayingFieldWithMinesweeperFieldValues method works properly");
  }

  @Test
  public void checkInitIntermediatePlayingFieldWithBombNumbersMethod() {
    System.out.println("Field before initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out.println("Print in method");
    minesweeperController.initIntermediatePlayingFieldWithBombNumbers();
    assertNotNull(minesweeperController.getIntermediatePlayingField());
    System.out.println("Field after initialisation");
    minesweeperView.printMinesweeperField(minesweeperController.getIntermediatePlayingField());
    System.out.println("initIntermediatePlayingFieldWithBombNumbers method works properly");
  }

  @RepeatedTest(3)
  public void checkGetNumberOfBombsAroundMethod()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    int numberOfBombs = 3;
    int IIndex = 1;
    int JIndex = 2;
    minesweeperController.initIntermediatePlayingFieldWithMinesweeperFieldValues();
    Class<? extends MinesweeperController> aClass = minesweeperController.getClass();
    Method getNumberOfBombsAround = aClass
        .getDeclaredMethod("getNumberOfBombsAround", int.class, int.class);
    getNumberOfBombsAround.setAccessible(true);
    assertEquals(numberOfBombs,
        getNumberOfBombsAround.invoke(minesweeperController, IIndex, JIndex));
    System.out.println("getNumberOfBombsAround method works properly");
  }

  @Test
  public void checkIsBombPresentMethod()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    int bombPresentInIndexI = 2;
    int bombPresentInIndexJ = 3;
    System.out.println("Print in method");
    minesweeperController.initIntermediatePlayingFieldWithMinesweeperFieldValues();
    final Class<? extends MinesweeperController> aClass = minesweeperController.getClass();
    final Method isBombPresent = aClass.getDeclaredMethod("isBombPresent", int.class, int.class);
    isBombPresent.setAccessible(true);
    assertEquals(1,
        isBombPresent.invoke(minesweeperController, bombPresentInIndexI, bombPresentInIndexJ));
    System.out.println("isBombPresent method works properly");
  }

}
