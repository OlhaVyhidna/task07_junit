package vyhidna.junit.plateau;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class PlateauTest {

  private static Plateau plateau;
  private static Plateau mockPlateau;
  private static Integer[] arrayOfIntegers;

  @BeforeAll
  public static void initFields() {
    arrayOfIntegers = new Integer[]{22, 4444, 7777, 6666, 555, 999};
    plateau = new Plateau(arrayOfIntegers);
    mockPlateau = mock(Plateau.class);
  }

  @Test
  public void checkFindSequenceMethod() {
    System.out.println("Plateau before searching");
    System.out.println(plateau.toString());
    plateau.findSequence();
    System.out.println("Plateau after searching");
    System.out.println(plateau.toString());
  }

  @Test
  public void checkInitSequenceMethod()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    int indexOfNumber = 1;
    Class<? extends Plateau> aClass = plateau.getClass();
    System.out.println("Plateau before sequence initialization");
    System.out.println(plateau.toString());
    Method initSequence = aClass.getDeclaredMethod("initSequence", int.class);
    initSequence.setAccessible(true);
    initSequence.invoke(plateau, indexOfNumber);
    System.out.println("Plateau after sequence initialization");
    System.out.println(plateau.toString());
  }

  @Test
  public void checkFindSequenceLengthMethod()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    String numberInString = "111";
    int expectedLength = 3;
    Class<? extends Plateau> aClass = plateau.getClass();
    Method findSequenceLength = aClass.getDeclaredMethod("findSequenceLength", String.class);
    findSequenceLength.setAccessible(true);
    int invoke = (int) findSequenceLength.invoke(plateau, numberInString);
    assertEquals(expectedLength, invoke);
  }

}
