package vyhidna.junit.minesweeper.model;


import java.util.Random;

public class Minesweeper {

  private final int M;
  private final int N;
  private final int p;
  private char[][] minesweeperField;

  public Minesweeper(int m, int n, int p) {
    M = m;
    N = n;
    this.p = p;
    minesweeperField = new char[M][N];
    initMinesweeperField();
  }


  private void initMinesweeperField(){
    char[][] localField = minesweeperField;
    for (int i = 0; i < localField.length; i++) {
      char[] booleans = localField[i];
      for (int j = 0; j < booleans.length; j++) {
           if(new Random().nextInt(p) != 0){
             minesweeperField[i][j]='*';
           }else {
             minesweeperField[i][j]='-';

           }
      }
    }
  }

  public char[][] getMinesweeperField() {
    return minesweeperField;
  }

  public void setMinesweeperField(char[][] minesweeperField) {
    this.minesweeperField = minesweeperField;
  }
}
