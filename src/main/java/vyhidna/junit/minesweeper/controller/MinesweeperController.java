package vyhidna.junit.minesweeper.controller;

import vyhidna.junit.minesweeper.model.Minesweeper;
import vyhidna.junit.minesweeper.view.MinesweeperView;

public class MinesweeperController {

  private MinesweeperView minesweeperView;
  private char[][] minesweeperField;
  private char[][] intermediatePlayingField;


  public MinesweeperController(Minesweeper minesweeper,
      MinesweeperView minesweeperView) {
    this.minesweeperView = minesweeperView;
    this.minesweeperField = minesweeper.getMinesweeperField();
    createIntermediatePlayingField();
    defaultInitIntermediatePlayingField();
  }

  private void createIntermediatePlayingField(){
    int currentM = minesweeperField.length + 2;
    int currentN = minesweeperField[minesweeperField.length - 1].length + 2;
    this.intermediatePlayingField = new char[currentM][currentN];
  }

  private void defaultInitIntermediatePlayingField() {
    for (int i = 0; i < intermediatePlayingField.length; i++) {
      char[] chars = intermediatePlayingField[i];
      for (int j = 0; j < chars.length; j++) {
        intermediatePlayingField[i][j] = '-';
      }
    }
  }

  public void initIntermediatePlayingFieldWithBombNumbers() {
    initIntermediatePlayingFieldWithMinesweeperFieldValues();
    for (int i = 0; i < minesweeperField.length; i++) {
      char[] chars = minesweeperField[i];
      for (int j = 0; j < chars.length; j++) {
        if (chars[j]=='-'){
          minesweeperField[i][j] = (char) (getNumberOfBombsAround(i+1, j+1)+48);
        }
      }
    }
    minesweeperView.printMinesweeperField(minesweeperField);
  }

  public void initIntermediatePlayingFieldWithMinesweeperFieldValues() {
    for (int i = 0; i < minesweeperField.length; i++) {
      char[] booleans = minesweeperField[i];
      for (int j = 0; j < booleans.length; j++) {
        if(minesweeperField[i][j]=='*'){
          intermediatePlayingField[i+1][j+1] = '*';
        }
      }
    }
    minesweeperView.printMinesweeperField(minesweeperField);
  }

  private int getNumberOfBombsAround(int i, int j){
    int numberOfBombs = 0;
    int top = i-1;
    int down = i+1;
    int left = j-1;
    int right = j+1;
    //left top diagonal neighbor
    numberOfBombs += isBombPresent(top, left);
    //top neighbor
    numberOfBombs += isBombPresent(top, j);
    //right top diagonal neighbor
    numberOfBombs += isBombPresent(top, right);
    //left neighbor
    numberOfBombs += isBombPresent(i, left);
    //right neighbor
    numberOfBombs += isBombPresent(i, right);
    //left down diagonal neighbor
    numberOfBombs += isBombPresent(down, left);
    //down neighbor
    numberOfBombs += isBombPresent(down, j);
    //down right diagonal neighbor
    numberOfBombs += isBombPresent(down, right);
    return numberOfBombs;
  }

  private int isBombPresent(int i, int j){
    if (intermediatePlayingField[i][j]=='*'){
      return 1;
    }
    return 0;
  }

  public char[][] getIntermediatePlayingField() {
    return intermediatePlayingField;
  }

  public void setIntermediatePlayingField(char[][] intermediatePlayingField) {
    this.intermediatePlayingField = intermediatePlayingField;
  }

  public char[][] getMinesweeperField() {
    return minesweeperField;
  }

  public void setMinesweeperField(char[][] minesweeperField) {
    this.minesweeperField = minesweeperField;
  }
}
