package vyhidna.junit.minesweeper.view;

public class MinesweeperView {

  public  void printMinesweeperField(char[][] minesweeperField) {
    for (int i = 0; i < minesweeperField.length; i++) {
      char[] chars = minesweeperField[i];
      for (int j = 0; j < chars.length; j++) {
        System.out.print(chars[j] + " ");
      }
      System.out.println();
    }
  }

}
