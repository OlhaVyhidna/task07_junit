package vyhidna.junit.minesweeper;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vyhidna.junit.minesweeper.controller.MinesweeperController;
import vyhidna.junit.minesweeper.model.Minesweeper;
import vyhidna.junit.minesweeper.view.MinesweeperView;

public class Main {
  private static Scanner scanner = new Scanner(System.in);
  private static final Logger LOG = LogManager.getLogger(Main.class);
  private static  Minesweeper minesweeper;
  private static  MinesweeperView minesweeperView;
  private static MinesweeperController minesweeperController;
  private static final String STRING_FOR_TEST = "String for test";

  public static void main(String[] args) {
    initFields();
    minesweeperController.initIntermediatePlayingFieldWithBombNumbers();
  }

  public  static  void  initFields(){
    minesweeper = initMinesweeper();
    minesweeperView = new MinesweeperView();
    minesweeperController = new MinesweeperController(minesweeper, minesweeperView);
  }

  private static Minesweeper initMinesweeper(){
    LOG.info("Enter minesweeper game field height");
    int m = scanner.nextInt();
    LOG.info("Enter minesweeper game field length");
    int n = scanner.nextInt();
    LOG.info("Enter minesweeper game field probability of a bomb");
    int p = scanner.nextInt();
    return new Minesweeper(m, n, p);
  }

  public Scanner getScanner() {
    return scanner;
  }

  public void setScanner(Scanner scanner) {
    Main.scanner = scanner;
  }
}
